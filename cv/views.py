from django.views.generic import TemplateView

class aboutMe(TemplateView):
    template_name ='aboutMe.html'

class experience(TemplateView):
    template_name = 'experience.html'

class portfolio(TemplateView):
    template_name = 'portfolio.html'

class contactMe(TemplateView):
    template_name = 'contactMe.html'
    
class secondPage(TemplateView):
    template_name = 'secondPage.html'

