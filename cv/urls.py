
from django.urls import path

from .views import*

app_name = 'cv'

urlpatterns = [
    path('', aboutMe.as_view(), name='aboutMe'),
    path('aboutMe/', aboutMe.as_view(), name = 'aboutMe'),
    path('experience/', experience.as_view(), name='experience'),
    path('portfolio/', portfolio.as_view(), name='portfolio'),
    path('contactMe/', contactMe.as_view(), name='contactMe'),
    path('secondPage/', secondPage.as_view(), name='secondPage'),

]