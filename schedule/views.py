from django.shortcuts import render, HttpResponseRedirect, redirect
from django.http import HttpResponse

from .forms import ScheduleForm
from .models import Schedule

import datetime

# Create your views here.
def landing(request):
    return render(request, 'mainSchedule.html')

def inputPage(request):
    form = ScheduleForm()

    if request.method == "POST":
        form = ScheduleForm(request.POST)

        print(form.errors)
        if form.is_valid():
            day, month, year = map(int, request.POST['dateForm'].split('-'))
            Schedule.objects.create(
                category = request.POST['categoryForm'],
                event = request.POST['eventForm'],
                place = request.POST['placeForm'],
                day = datetime.date(day, month, year).strftime('%A'),
                date = request.POST['dateForm'],
                jam = request.POST['timeForm']
            )

            return HttpResponseRedirect('sched')

        
       

    context = {
        'form':form
    }
    return render(request, "inputPage.html",context)


def sched(request):
    schedData = Schedule.objects.all()
    context = {
        'CONTENT': schedData
    }
    return render(request, 'schedule.html', context)


def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
    return sched(request)



