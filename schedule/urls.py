from django.urls import path, re_path
from .views import *

app_name = "schedule"

urlpatterns = [
    path('', landing),
    path('landing', landing, name='landing'),
    path('delete', delete),
    path('create', inputPage),
    path('sched', sched, name='sched')
]
