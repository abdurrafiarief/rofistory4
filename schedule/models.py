from django.db import models
import datetime
# Create your models here.

class Schedule(models.Model):
    category = models.CharField(max_length=50, default="Random")
    event = models.CharField(max_length = 50)
    place = models.CharField(max_length = 50)
    day = models.CharField(max_length = 20)
    date = models.DateField()
    jam = models.TimeField()

    def __str__(self):
        return self.event

    def date_day(self):
        new_tanggal = str(self.date)
        year, month, day = map(int,new_tanggal.split('-'))

        new_month = datetime.date(year, month, day).strftime("%B")

        if(day == 1):
            new_day = "1st"
        elif(day == 2):
            new_day = "2nd"
        elif(day==3):
            new_day = "3rd"
        else:
            new_day = str(day) + "th"

        return self.day + ', ' + str(new_month) + " " + str(new_day) + " " + str(year) 


    def time(self):
        return str(self.jam)