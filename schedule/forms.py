from django import forms

from .models import Schedule

class ScheduleForm(forms.Form):

    categoryForm = forms.CharField(label = "category", help_text = 'Please input the category of the event')

    eventForm = forms.CharField(label = 'Event',
    help_text = 'Please input the event')

    placeForm = forms.CharField(label='Place', required=False)

    dateForm = forms.CharField(label='Date', help_text = 'Enter a date in MM-DD-YYYY format')

    timeForm = forms.TimeField(label='Time', help_text = 'Entar a time in Hour:Minute:Seconds format')



